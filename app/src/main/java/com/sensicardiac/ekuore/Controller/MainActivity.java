package com.sensicardiac.ekuore.Controller;

import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;


import com.sensicardiac.ekuore.R;
import com.sensicardiac.ekuore.Waveform.WaveFormView1_1;
import com.sensicardiac.ekuore.Wifi_Controls.WifiScanReceiver;
import com.sensicardiac.ekuore.Wifi_Controls.Wifi_Switch;

import static com.sensicardiac.ekuore.Variables.DSP_Variables.startDSP;
import static com.sensicardiac.ekuore.Variables.Globals.app_context;
import static com.sensicardiac.ekuore.Variables.Globals.wifi_frag;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.adapter;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.lv_1;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.lv_2;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifi;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifiReciever;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifispinnerArray;
import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static WaveFormView1_1 wavform;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv_1 = (Spinner) findViewById(R.id.spinner_wifi);
        lv_2 = (Spinner)findViewById(R.id.spinner_stet);
        app_context = getApplicationContext();
        WifiManager wifiManager = (WifiManager)app_context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED){
        wifiManager.setWifiEnabled(true);
            try {
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
        wifiReciever = new WifiScanReceiver();

        wifi.startScan();
        startDSP();
        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_setup){
            wifi_frag = Wifi_Switch.newInstace();
            try{
                if (wifispinnerArray.size() > 0 ){
            show_Fragment_Dialog(wifi_frag,"Wifi Settings");}
            else {
                    wifi.startScan();
                    try {
                        wait(3000);
                        show_Fragment_Dialog(wifi_frag,"Wifi Settings");
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                        Log.d("Catch","e1 : "+ e1);
                    }

                }}
            catch (Exception e){
                wifi.startScan();
                try {
                    sleep(5000);
                    show_Fragment_Dialog(wifi_frag,"Wifi Settings");
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void show_Fragment_Dialog(DialogFragment frag, String ftitle){
        FragmentManager fm = getSupportFragmentManager();
        frag.show(fm,ftitle);
    }
    protected void onPause() {
        unregisterReceiver(wifiReciever);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }
}
