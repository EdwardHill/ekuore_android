package com.sensicardiac.ekuore.File_Saver;

import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;



/**
 * Created by Edward on 16/08/17.
 */
public class Audio_File_Saver {
    public static void Save_Audio(byte[] byte_packets){
        WaveHeader hdr = null;

            hdr = new WaveHeader(WaveHeader.FORMAT_PCM, (short) 1, 4000, (short) 16, byte_packets.length);

        File temp = new File("/sdcard/sensi_x/tempfile.wav");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensi_x");
        if (!dir_exists(dir.getPath())){
            File directory = new File(String.valueOf(dir));
            directory.mkdirs();
        }
        if (!temp.exists()){
            try {
                temp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (temp.exists()){
            temp.delete();
            try {
                temp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(temp);
            hdr.write(fos);
            fos.write(byte_packets);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static boolean dir_exists(String dir_path)
    {
        boolean ret = false;
        File dir = new File(dir_path);
        if(dir.exists() && dir.isDirectory())
            ret = true;
        return ret;
    }
}
