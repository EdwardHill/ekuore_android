package com.sensicardiac.ekuore.Wifi_Controls;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.sensicardiac.ekuore.R;

import java.util.List;

import static android.content.Context.WIFI_SERVICE;
import static com.sensicardiac.ekuore.Variables.Globals.app_context;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.Stethoscope_1;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.Stethoscope_1_SSID;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.adapter;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.lv_1;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.lv_2;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.stetId;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifi;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifiScanList;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifiScanList2;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifispinnerArray;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifispinnerArray2;

/**
 * Created by Edward on 2016/10/26.
 */

public class Stet_Connect extends DialogFragment {
    public static Stet_Connect newInstace(){
        return new Stet_Connect();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v = inflater.inflate(R.layout.fragment_stethoscope, container, false);
        lv_2 = (Spinner)v.findViewById(R.id.spinner_stet);

        Button Connect = (Button)v.findViewById(R.id.button_stet_connect);
        Button Scan = (Button)v.findViewById(R.id.button_scanner);
        Connect.setText("Connect to Selected Stethoscope");
        adapter = new ArrayAdapter<String>(
                app_context, R.layout.simple_spinner_item1, wifispinnerArray);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item1);
        lv_2.setAdapter(adapter);
//        ConnectivityManager connManager = (ConnectivityManager) app_context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);



        Connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stethoscope_1 = lv_2.getSelectedItemPosition();
                Stethoscope_1_SSID = String.valueOf(lv_2.getSelectedItem());
                WifiManager wifiManager = (WifiManager)app_context.getSystemService(WIFI_SERVICE);
////remember id
                stetId =0;
                List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
//                int netId = wifiManager.addNetwork(wifiConfig);
                for (int i =0;i < configs.size();i++){
                    if (configs.get(i).SSID.contains(Stethoscope_1_SSID)){

                            wifiManager.setWifiEnabled(true);

                        stetId = i;
                        i = configs.size()+1;}}
                wifiManager.disconnect();
//                wifiManager.reassociate();
                wifiManager.enableNetwork(stetId, true);

                wifiManager.reconnect();
                dismiss();
            }
        });
        Scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wifi.startScan();

                try {
                    adapter = new ArrayAdapter<String>(
                            app_context, R.layout.simple_spinner_item1, wifispinnerArray);
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item1);
                    lv_2.setAdapter(adapter);
                }catch (Exception e){}

            }
        });
        return v;
    }
}
