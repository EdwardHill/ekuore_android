package com.sensicardiac.ekuore.Wifi_Controls;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


import android.widget.ToggleButton;

import com.sensicardiac.ekuore.Controller.MainActivity;
import com.sensicardiac.ekuore.R;

import java.util.List;

import static android.content.Context.WIFI_SERVICE;
import static com.sensicardiac.ekuore.Variables.Globals.app_context;
import static com.sensicardiac.ekuore.Variables.Globals.stet_frag;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.Network_1;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.Network_1_SSID;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.adapter;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.lv_1;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.netId;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifiScanList;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.wifispinnerArray;

/**
 * Created by Edward on 2016/10/26.
 */

public class Wifi_Switch extends DialogFragment {
//    public Wifi_Switch(){}
    public static Wifi_Switch newInstace(){
        return new Wifi_Switch();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v = inflater.inflate(R.layout.fragment_wifi, container, false);
        lv_1 = (Spinner)v.findViewById(R.id.spinner_wifi);
        final ToggleButton tog = (ToggleButton)v.findViewById(R.id.toggle_wifi_on_off);
        Button Connect = (Button)v.findViewById(R.id.button_wifi_connect);
        Connect.setText("Connect to Selected Network");
        adapter = new ArrayAdapter<String>(
                app_context, R.layout.simple_spinner_item1, wifispinnerArray);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item1);
        lv_1.setAdapter(adapter);
        ConnectivityManager connManager = (ConnectivityManager) app_context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            tog.setChecked(true);
            tog.setText("Wifi is ON");
        }
        else {tog.setChecked(false);
            tog.setText("Wifi is OFF, Turn ON?");}
        tog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiManager wifiManager = (WifiManager)app_context.getSystemService(WIFI_SERVICE);
                if (!tog.isChecked()){

                    wifiManager.setWifiEnabled(false);
                    tog.setChecked(false);
                    tog.setText("Wifi is OFF, Turn ON?");
                }
                else {
                    wifiManager.setWifiEnabled(true);
                    tog.setChecked(true);
                    tog.setText("Wifi is ON");
                }
            }
        });
        Connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), String.valueOf(lv_1.getSelectedItemPosition()), Toast.LENGTH_LONG).show();
                Network_1 = lv_1.getSelectedItemPosition();
                Network_1_SSID = String.valueOf(lv_1.getSelectedItem());
                WifiManager wifiManager = (WifiManager)app_context.getSystemService(WIFI_SERVICE);
                netId =0;
                List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
                for (int i =0;i < configs.size();i++){
                    if (configs.get(i).SSID.contains(Network_1_SSID)){
                        netId = i;
                        i = configs.size()+1;}}
                wifiManager.disconnect();

                wifiManager.enableNetwork(netId, true);
                wifiManager.reconnect();
                dismiss();
                stet_frag = Stet_Connect.newInstace();
                show_Fragment_Dialog(stet_frag,"Start");
            }
        });
        return v;
    }

    public void show_Fragment_Dialog(DialogFragment frag, String ftitle){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        frag.show(fm,ftitle);
    }
}
