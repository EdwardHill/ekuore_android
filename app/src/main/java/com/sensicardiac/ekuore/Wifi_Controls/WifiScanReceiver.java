package com.sensicardiac.ekuore.Wifi_Controls;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.sensicardiac.ekuore.Variables.Globals.app_context;
import static com.sensicardiac.ekuore.Variables.Wifi_Variables.*;


public class WifiScanReceiver extends BroadcastReceiver {

    public List<ScanResult> results;
    public  int size = 0;
        public void onReceive(Context c, Intent intent) {
            try {
                if (wifiScanList != null){
                    if (wifiScanList.size() > 0){
                        wifiScanList.clear();}
                    if (wifiScanList2.size() >0){
                        wifiScanList2.clear();}
                    if (wifispinnerArray.size() >0){
                        wifispinnerArray.clear();}
                    if (wifispinnerArray2.size() > 0){
                        wifispinnerArray2.clear();}}

                wifis = null;
                wifis2 = null;
                wifiScanList = wifi.getScanResults();
                wifiScanList2 = wifi.getScanResults();
                wifis = new String[wifiScanList.size()];
                wifis2 = new String[wifiScanList2.size()];
                results = wifi.getScanResults();
                wifispinnerArray =  new ArrayList<String>();
                wifispinnerArray2 =  new ArrayList<String>();
                size = results.size();
                for(int i = 0; i < size; i++){
                    Log.e("SSID",results.get(i).SSID);
                    wifis[i] = ((wifiScanList.get(i)).SSID);
                    wifis2[i] = ((wifiScanList2.get(i)).SSID);
                    wifispinnerArray.add(wifis[i]);
                    wifispinnerArray.add(wifis2[i]);
                }}catch (Exception e){}
        }


    }