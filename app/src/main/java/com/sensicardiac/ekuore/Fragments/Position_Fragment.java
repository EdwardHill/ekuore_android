package com.sensicardiac.ekuore.Fragments;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.sensicardiac.ekuore.R;




public class Position_Fragment extends Fragment {

    public Position_Fragment() {
        // Required empty public constructor
    }


    public static ToggleButton Aortic_Toggle;
    public static ToggleButton Pulmonary_Toggle;
    public static ToggleButton Tricuspid_Toggle;
    public static ToggleButton Apex_Toggle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_position, container, false);
        Aortic_Toggle = (ToggleButton) v.findViewById(R.id.toggle_aortic);
        Pulmonary_Toggle = (ToggleButton) v.findViewById(R.id.toggle_pulmonary);
        Tricuspid_Toggle = (ToggleButton)v.findViewById(R.id.toggle_tricuspid);
        Apex_Toggle = (ToggleButton)v.findViewById(R.id.toggle_apex);
        Aortic_Toggle.setChecked(true);



        Aortic_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Aortic_Toggle.isChecked()){
                    Pulmonary_Toggle.setChecked(false);
                    Tricuspid_Toggle.setChecked(false);
                    Apex_Toggle.setChecked(false);
                    Selector_Fragment.Aortic_Radio.setChecked(true);
                    Selector_Fragment.Pulmonary_Radio.setChecked(false);
                    Selector_Fragment.Tricuspid_Radio.setChecked(false);
                    Selector_Fragment.Apex_Radio.setChecked(false);

                }
            }
        });
        Pulmonary_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Pulmonary_Toggle.isChecked()){
                    Aortic_Toggle.setChecked(false);
                    Tricuspid_Toggle.setChecked(false);
                    Apex_Toggle.setChecked(false);
                    Selector_Fragment.Aortic_Radio.setChecked(false);
                    Selector_Fragment.Pulmonary_Radio.setChecked(true);
                    Selector_Fragment.Tricuspid_Radio.setChecked(false);
                    Selector_Fragment.Apex_Radio.setChecked(false);
                }
            }
        });
        Tricuspid_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tricuspid_Toggle.isChecked()){
                    Pulmonary_Toggle.setChecked(false);
                    Aortic_Toggle.setChecked(false);
                    Apex_Toggle.setChecked(false);
                    Selector_Fragment.Aortic_Radio.setChecked(false);
                    Selector_Fragment.Pulmonary_Radio.setChecked(false);
                    Selector_Fragment.Tricuspid_Radio.setChecked(true);
                    Selector_Fragment.Apex_Radio.setChecked(false);
                }
            }
        });
        Apex_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Apex_Toggle.isChecked()){
                    Pulmonary_Toggle.setChecked(false);
                    Tricuspid_Toggle.setChecked(false);
                    Aortic_Toggle.setChecked(false);
                    Selector_Fragment.Aortic_Radio.setChecked(false);
                    Selector_Fragment.Pulmonary_Radio.setChecked(false);
                    Selector_Fragment.Tricuspid_Radio.setChecked(false);
                    Selector_Fragment.Apex_Radio.setChecked(true);
                }
            }
        });
        return v;
    }}


