package com.sensicardiac.ekuore.Fragments;



import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import  android.support.v4.app.Fragment;

import com.sensicardiac.ekuore.R;


public class Selector_Fragment extends Fragment {


    public Selector_Fragment() {
        // Required empty public constructor
    }




    public static RadioButton Aortic_Radio;
    public static RadioButton Pulmonary_Radio;
    public static RadioButton Tricuspid_Radio;
    public static RadioButton Apex_Radio;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_selector, container, false);
        Aortic_Radio = (RadioButton)v.findViewById(R.id.radio_aortic);
        Pulmonary_Radio = (RadioButton)v.findViewById(R.id.radio_pulmonary);
        Tricuspid_Radio = (RadioButton)v.findViewById(R.id.radio_tricuspid);
        Apex_Radio = (RadioButton)v.findViewById(R.id.radio_apex);
        Aortic_Radio.setChecked(true);
        Aortic_Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aortic_Radio.setChecked(true);
                Pulmonary_Radio.setChecked(false);
                Tricuspid_Radio.setChecked(false);
                Apex_Radio.setChecked(false);
                Position_Fragment.Aortic_Toggle.setChecked(true);
                Position_Fragment.Pulmonary_Toggle.setChecked(false);
                Position_Fragment.Tricuspid_Toggle.setChecked(false);
                Position_Fragment.Apex_Toggle.setChecked(false);
            }
        });
        Pulmonary_Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aortic_Radio.setChecked(false);
                Pulmonary_Radio.setChecked(true);
                Tricuspid_Radio.setChecked(false);
                Apex_Radio.setChecked(false);
                Position_Fragment.Aortic_Toggle.setChecked(false);
                Position_Fragment.Pulmonary_Toggle.setChecked(true);
                Position_Fragment.Tricuspid_Toggle.setChecked(false);
                Position_Fragment.Apex_Toggle.setChecked(false);
            }
        });
        Tricuspid_Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aortic_Radio.setChecked(false);
                Pulmonary_Radio.setChecked(false);
                Tricuspid_Radio.setChecked(true);
                Apex_Radio.setChecked(false);
                Position_Fragment.Aortic_Toggle.setChecked(false);
                Position_Fragment.Pulmonary_Toggle.setChecked(false);
                Position_Fragment.Tricuspid_Toggle.setChecked(true);
                Position_Fragment.Apex_Toggle.setChecked(false);
            }
        });
        Apex_Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aortic_Radio.setChecked(false);
                Pulmonary_Radio.setChecked(false);
                Tricuspid_Radio.setChecked(false);
                Apex_Radio.setChecked(true);
                Position_Fragment.Aortic_Toggle.setChecked(false);
                Position_Fragment.Pulmonary_Toggle.setChecked(false);
                Position_Fragment.Tricuspid_Toggle.setChecked(false);
                Position_Fragment.Apex_Toggle.setChecked(true);
            }
        });
        return v;
    }



}
