package com.sensicardiac.ekuore.DSP;

/**
 * Created by davidfourie on 2016/09/30.
 */

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;



import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.List;

//import static com.sensicardiac.sensi_x_android.File_Saver.Audio_File_Saver.Save_Audio;

public class Offline_downsampler extends AsyncTask<Void, Integer, Void> {

    @Override
    protected void onPreExecute(){
//        MainDisplay.Record_Stop.setEnabled(false);
//        MainDisplay.Play_Pause.setEnabled(false);
//        MainDisplay.Send_Mail.setEnabled(false);
//        MainDisplay.Download.setEnabled(false);
//        MainDisplay.prog_text.setText("Saving File : 0 %");
//        MainDisplay.save_prog.setProgress(0);
//        MainDisplay.save_prog.setVisibility(View.VISIBLE);
//        MainDisplay.prog_text.setVisibility(View.VISIBLE);
    }
    @Override
    protected void onPostExecute(Void params)
    {
        Log.d("Processing","Saving audio ");
//        Save_Audio(output);
//        MainDisplay.Record_Stop.setEnabled(true);
//        MainDisplay.Play_Pause.setEnabled(true);
//        MainDisplay.Send_Mail.setEnabled(true);
//        MainDisplay.Download.setEnabled(true);
//        MainDisplay.save_prog.setVisibility(View.GONE);
//        MainDisplay.prog_text.setVisibility(View.GONE);
//        com.sensicardiac.sensi_x_android.Controller.MainDisplay.final_graph();
    }

    @Override
    protected void onProgressUpdate(Integer... params)
    {
        int val = params[0];
        val *= 10;
//        MainDisplay.save_prog.setProgress(val);
//        MainDisplay.prog_text.setText("Saving File : " + val + "%");
//        Log.d("Processing","progress: "+params[0]);
    }

    @Override
    protected Void doInBackground(Void... params) {

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        short[] input2=new short[input.length/4];
        double[] input_temp=new double[input.length/4];
        int input_count=0;
        for(int i=0;i<input.length;i++)
        {
            if(i%4==0)
            {
                bb.put(0,input[i]);
                bb.put(0,input[i+1]);
                input2[input_count]=bb.get(0);
                input_count++;
            }
        }

        antiAliasing1.reset();
        for(int i=0;i<input2.length;i++)
        {
            input_temp[i]=antiAliasing1.add_val(input2[i]);
        }
        antiAliasing1.reset();
        for(int i=input2.length-1;i>=0;i--)
        {
            input2[i]=(short)Math.round(antiAliasing1.add_val(input_temp[i]));
        }

        antiAliasing.reset();

//        Log.d("Offline downsample","original length: "+input2.length+" time: "+input2.length/44100.0);
        //add filtered values to list.
        List<Double> full=new LinkedList<>();
        int count=0;
        int count2=0;
        input_count=0;
        double[] output1= new double[100*4000];
        double temp=0;
        int timer=(int)Math.floor(input2.length/10);
        int timer_count=0;
        for(int i=0;i<input2.length;i++)
        {
            temp=antiAliasing.add_val(input2[i]);
            count++;
            count2++;
            if(count==M-1)
            {
//                output1.add();
                output1[input_count]=temp;
                input_count++;
                count=0;

            }
            for(int j=0;j<L-1;j++)
            {
                temp=antiAliasing.add_val(0d);
                count++;
                count2++;
                if(count==M-1)
                {
                    output1[input_count]=temp;
                    input_count++;

                    count=0;
                }
            }  if(i%timer==0)
        {
            publishProgress(timer_count);
            timer_count++;
        }



        }
        input2=null;
        Log.d("Offline downsample","List allocated");

        //reverse filter list;

        //downsample


        full=null;
        //cast to output
        double[] output2=new double[input_count];
        short[] output3=new short[input_count];
        for(int i=0;i<output2.length;i++)
        {
            output2[i]=800*antiAliasing2.add_val(output1[i]);
        }
        antiAliasing2.reset();
        for(int i=output2.length-1;i>=0;i--)
        {
            output2[i]=antiAliasing2.add_val(output2[i]);
        }
        antiAliasing2.reset();
        for(int i=0;i<output2.length;i++)
        {
            output3[i]=(short)Math.round(16*antiAliasing2.add_val(output2[i]));
        }



        output1=null;
        Log.d("Offline downsample","downsampled length: "+output2.length+" time: "+output2.length/4000.0);
        ByteBuffer bb2=ByteBuffer.allocate(2*output2.length);
        bb2.order(ByteOrder.LITTLE_ENDIAN);
        bb2.asShortBuffer().put(output3);
        this.output=bb2.array();

        return null;
    }

    private Filter antiAliasing;
    private Filter antiAliasing1;
    private Filter antiAliasing2;
    private int L;
    private int M;
    private byte[] input;
    private byte[] output;

    public Offline_downsampler(){
        double[] B= new double[3];
        double[] A= new double[3];
        B[0]=7.91946614e-07;
        B[1]=1.58389323e-06;
        B[2]=7.91946614e-07;

        A[0]=1;
        A[1]=-1.99748136;
        A[2]=0.99748453;
        L=40;
        M=441;
        antiAliasing=new Filter(A,B);


        B[0]=0.00120741;
        B[1]=0.00241481;
        B[2]=0.00120741;

        A[0]=1;
        A[1]=-1.89933342;
        A[2]=0.90416304;

        antiAliasing1=new Filter(A,B);

        antiAliasing2=new Filter(Filter.filterType.bandpass50_150);


    }
    public void doDownsample(byte[] input)
    {
        this.input=input;
        this.execute();

    }

}
