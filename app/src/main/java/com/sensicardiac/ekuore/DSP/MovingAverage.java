//package com.sensicardiac.sensi_module.dsp;
package com.sensicardiac.ekuore.DSP;
/**
 * Created by davidfourie on 16/05/18.
 */
public class MovingAverage {
    private double mov_aveN; //moving average times N
    private int N;//length of which to average over
    private long count;
    private boolean ready=false;
    public void reset()
    {
        mov_aveN=0;
        count=0;

    }
    public void add_val(double x)
    {
        ready=count<N;
        if(ready)
        {
            count++;
        }

        mov_aveN=mov_aveN+x-mov_aveN/count; //update the moving average
    }
    public double getMovingAverage()
    {
        return mov_aveN/count;
    }

    public MovingAverage(int n) {
        N = n;
        count=0;
    }
    public boolean isReady()
    {
        return !ready;
    }
}
