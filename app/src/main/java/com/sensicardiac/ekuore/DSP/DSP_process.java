package com.sensicardiac.ekuore.DSP;
import java.util.Timer;
import java.util.TimerTask;

public class DSP_process{
    private Timer timer;
    public static RunningStatistics stats;

    Sound_detecter sd;
    public DSP_process()
    {
	    stats=new RunningStatistics();
    sd= new Sound_detecter(4000,0.5);
    timer= new Timer();
    sd.reset();
    timer.schedule(new doProcess(),600,50);


    }
    public void reset()
    {
        sd.reset();
	stats.reset();
    }
    public void add_val(double x)
    {
//        return 70;
//        return ;
	    double hr=sd.add_val(x);
	    
	stats.add_val(hr);
    }
    public class doProcess extends TimerTask{
            public void run()
            {
//               System.out.println("Running"); 
           sd.process();
            }

    }

}
