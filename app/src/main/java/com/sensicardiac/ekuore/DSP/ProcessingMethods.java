/**
 * Created by davidfourie on 2016/02/03.
 */

package com.sensicardiac.ekuore.DSP;
//import android.util.Log;

import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

public class ProcessingMethods {

    public static void gaussianiir1d(Double data[], int length, double sigma, int numsteps)
    {
        double lambda, dnu;
        float nu, boundaryscale, postscale;
        int i;
        int step;

        lambda = (sigma*sigma)/(2.0*numsteps);
        dnu = (1.0 + 2.0*lambda - Math.sqrt(1.0 + 4.0*lambda))/(2.0*lambda);
        nu = (float)dnu;
        boundaryscale = (float)(1.0/(1.0 - dnu));
        postscale = (float)(Math.pow(dnu/lambda,numsteps));

        for(step = 0; step < numsteps; step++)
        {
            data[0] *= boundaryscale;

        /* Filter rightwards (causal) */
            for(i = 1; i < length; i++)
                data[i] += nu * data[i - 1];

            data[i = length - 1] *= boundaryscale;

        /* Filter leftwards (anti-causal) */
            for(; i > 0; i--)
                data[i - 1] += nu*data[i];
        }

        for(i = 0; i < length; i++)
            data[i] *= postscale;

        return;
    }
    public static void performFFTConvolution(Double in[])
    {
        DoubleFFT_1D dFFT= new DoubleFFT_1D(in.length);
        double [] temp=new double[in.length*2];
        for(int i=0;i<in.length;i++)
        {

            temp[i]=in[i]*Math.pow(hammingFunction(i, in.length),2);
//	    temp[i]=in[i];
        }
        dFFT.realForwardFull(temp);

        for(int i=0;i<in.length;i++)
        {


           temp[2*i]=Math.sqrt(temp[2*i]*temp[2*i]+temp[2*i+1]*temp[2*i+1]);
            temp[2*i+1]=0.0;

        }
//        Log.i("Realtime",":X: end");
        dFFT.complexInverse(temp,false);

        for(int i=0;i<in.length;i++)
        {
         in[i]= Math.sqrt(temp[2*i]*temp[2*i]+temp[2*i+1]*temp[2*i+1]);
        }
    }
    
     private static double hammingFunction(int n, int len) {
        double a = 0.54;
        double b = 0.46;
        return a - b * Math.cos((2 * Math.PI * n) / (len - 1));
    }
}


