package com.sensicardiac.ekuore.DSP;



import com.sensicardiac.ekuore.Variables.DSP_Variables;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by davidfourie on 2016/09/16.
 */

public class downsampler {
    private Filter antiAliasing;
    private int factor;
    private int count;
//    ByteBuffer output;

    private int output_count;
    //    private int graph_count;
//    private long graph_length;
//    private int graph_update;
//    private int[] graph;
    private Preprocessor ps;
    private long count2;




    public downsampler()
    {
//        ps=new Preprocessor(1000,4000);
//        graph=new int[graph_time*4000];
//        for(int i=0;i<graph.length;i++)
//        {
//            graph[i]=0;
//        }
//        graph_update=(int)Math.round(update_time*4000);
//
//        input=ByteBuffer.allocate(2);
        count=0;
        count2=0;
        output_count=0;
        double[] B= new double[3];
        double[] A= new double[3];
        factor=41;


        B[0]=0.00019897;
        B[1]=0.00039794;
        B[2]=0.00019897;

        A[0]=1;
        A[1]=-1.95970703;
        A[2]=0.96050292;

        antiAliasing=new Filter(A,B);
//        Log.i("Setup","Factor: "+factor);

    }

    public void add_val(byte LSB, byte MSB)
    {
//        input.clear();
//        input.put(MSB);
//        input.put(LSB);
        ByteBuffer input=ByteBuffer.allocate(2);
        input.order(ByteOrder.LITTLE_ENDIAN);
        input.put(LSB);
        input.put(MSB);


        double val=antiAliasing.add_val((double)input.getShort(0));
//        count2++;
        if(count==factor)
        {
//            Log.d("Counter2","X: "+output_count+" Y"+val);
//
            //doStuffhere with Val
//            Log.d("Before","X: "+output_count+" Y"+val);
//            val=ps.add_val(val*100)*128;
            DSP_Variables.hrCalc.add_val(val);
            short val_out=(short)Math.round(val);
//            output.putShort(2*output_count,val_out);

            //add data to graph
//            graph[graph_count]=val_out;
            //Graph timer trigger
//            if(graph_count%graph_update==0)
//            {
////                Log.d("After","X: "+output_count+" Y:");
//                setGraph_data_ready(true);
//            if(output_count%10==0)
//            {
//                Log.d("Counter","X: "+output_count);
//            }
//            Log.d("Downsample",output_count+" "+val);
//            }


            //reset counter to overide
//            if(graph_count==graph.length)
//            {
//                graph_count=0;
//            }


            output_count++;
            count=0;
//            Log.d("Counter","val: "+val);

        }
        else
        {
            count++;
        }
    }
    public void reset()
    {
        count2=0;
//        ps.reset();
        count=0;
        antiAliasing.reset();
//        for(int i=0;i<graph.length;i++)
//        {
//            graph[i]=0;
//        }
//        graph_count=0;
    }
//    public int[] getGraph()
//    {
//        return graph;
//



}
