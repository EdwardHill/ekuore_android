//package com.sensicardiac.sensi_module.dsp;
package com.sensicardiac.ekuore.DSP;
/**
 * Created by davidfourie on 16/05/25.
 */
public class VisualEnveloper {
    private Normaliser pre_norm;
    private Normaliser post_norm;



    private MovingAverage move_ave;
    private MovingAverage move_ave2;
    private Filter lowpass_10;
    private Filter input_filt;

    public VisualEnveloper() {

        pre_norm=new Normaliser(0.9,4000,0.1);
        post_norm=new Normaliser(0.95,4000,0.5);

        move_ave=new MovingAverage(2000);
        lowpass_10=new Filter(Filter.filterType.lowpass500);
        input_filt=new Filter(Filter.filterType.lowpass10);

//        System.out.println("Running");
    }
    public void reset()
    {
        pre_norm.reset();
        post_norm.reset();
        move_ave.reset();
        input_filt.reset();

    }
    public double add_val(double x)
    {
//        System.out.println("adding value");
//        move_ave2.add_val(x);
//        x=x-move_ave2.getMovingAverage();
	x=lowpass_10.add_val(x);
       
        if(x==0)
        {
            x=0.0000001;
        }
        x=Math.log(Math.abs(x));
	x=input_filt.add_val(x);
	x=Math.exp(x);
//        x=post_norm.add_val(x);
	move_ave.add_val(x);
	x=x-move_ave.getMovingAverage();
//	x=x*4;
        return x;

    }

}
