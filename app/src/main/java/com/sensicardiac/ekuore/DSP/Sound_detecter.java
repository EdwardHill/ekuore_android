package com.sensicardiac.ekuore.DSP;


import android.util.Log;

public class Sound_detecter{
//   private ShannonEnveloper sh;
//   private Peak_detecter sd;
//   private Peak_counter pc;

   private long counter=0;
   private long interval;
   private long interval_count=0;
   private long window=0;

   Signal_buffer signal_buffer;
   private int sampling_rate;
   private double last_peak_hr=0;
   private double last_hr=0;
   private int RR_low;
   private int RR_high;
   private int HR_low;
   private int HR_high;
   private MovingAverage hr_ave;

   private Preprocessor prep;
   private Filter lowpass;

     public Sound_detecter(int sampling_rate,double interval)
     {
     this.sampling_rate=sampling_rate;
//     sh=new test.ShannonEnveloper();
     this.interval=(long)Math.round(sampling_rate*interval);
     hr_ave=new MovingAverage(10);
     this.window=(long)Math.round(sampling_rate*3);
     prep=new Preprocessor(sampling_rate*3,sampling_rate);
     signal_buffer = new Signal_buffer((int) this.window);
     lowpass=new Filter(Filter.filterType.bandpass50_150);

//     sd.reset();
//     sh.reset();
     HR_low=50;
     HR_high=90;
     setRR();
     this.last_hr=(HR_low+HR_high)/2;

//     pc = new Peak_counter(4000,1.5);
     counter=0;

   }
   public void setRR()
   {
   RR_low= (int) Math.floor((60.0 / (HR_high)) * (sampling_rate+0.0));
   RR_high= (int) Math.floor((60.0 / (HR_low)) * (sampling_rate+0.0));
   }

   public void reset()
   {

       signal_buffer.reset();
       hr_ave.reset();
       counter=0;
        prep.reset();
       interval_count=0;
       HR_low=50;
	HR_high=90;
	lowpass.reset();
	setRR();
       this.last_hr=(HR_low+HR_high)/2;
       
   }

   public double add_val(double x)
   {

        double val=prep.add_val(lowpass.add_val(x));

       signal_buffer.add_val(Math.abs(val));
	
       interval_count++;
       counter++;
       return last_hr;
   }
   public void process()
   {
       Log.d("HRCalc","Timer click");
   while(interval_count>=interval){
        interval_count=interval_count-interval;
       Log.d("HRCalc","Interval click");
       if(signal_buffer.isReady())
        {
//            System.out.println("Processing start");
		
            Double[] buffer = signal_buffer.getArray();
              
            ProcessingMethods.performFFTConvolution(buffer);
            double max=0;
            int pos=0;
//            System.out.println("Buffer size: "+buffer.length);
            for(int i=RR_low;i<RR_high;i+=100)
            {
                double val=buffer[i];
                
//		if(counter>4*sampling_rate)
//		{
//			val=val*buffer[i]*taper(i,1,hr_ave.getMovingAverage());
//		}
                
                

//                
                if(val>max && val>buffer[i-100] && val>buffer[i+100])
                {
		    max=val;
			pos=i;
                }

            }
	    if(pos==0)
	    {
		     pos = (RR_low + RR_high) / 2;
	    }

            double hr=60.0*sampling_rate/pos;
            Log.d("HRCalc","HR = "+hr);
            last_hr=hr;
	    
            if(counter>5*sampling_rate)
            {
		    hr_ave.add_val(hr);
                
		
		HR_high=(int)hr_ave.getMovingAverage()+10;
		HR_low=(int)hr_ave.getMovingAverage()-10;
		if(HR_high<180 && HR_low>50)
		{
			setRR();
		}
		
		
            }
//	    System.out.println("Processing end");
//            System.out.println(hr);

        }
       





   }

}

 private double taper(double x,int n,double C)
    {
        return Math.exp(-Math.pow((x-C)/((RR_high-RR_low)*4),2));
    }
    }