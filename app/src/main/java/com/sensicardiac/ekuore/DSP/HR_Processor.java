package com.sensicardiac.ekuore.DSP;

import java.util.Timer;
import java.util.TimerTask;

//import java.lang.Thread;

public class HR_Processor{
    private Timer timer;
    private int counter;

    private Filter anti_aliasing;
    private doProcess mt;
    public RunningStatistics stats;
    public HR_Processor()
    {

        mt=new doProcess();
        anti_aliasing=new Filter(Filter.filterType.lowpass500);
        timer= new Timer();
        stats=new RunningStatistics();
        Process_signal.reset();
        Process_signal.setHR(50,90);
        Process_signal.reset();
        timer.schedule(new doProcess(),1,50);
//    mt.start();
        double last_hr;


    }
    public void reset()
    {
        anti_aliasing.reset();
        Process_signal.reset();
        counter=0;
        stats.reset();
    }
    public void add_val(double x)
    {

        Process_signal.l_add(x);
        stats=Process_signal.stats;

    }


    public class doProcess extends TimerTask{
        public void run()
        {
            Process_signal.process();
        }

    }

}
