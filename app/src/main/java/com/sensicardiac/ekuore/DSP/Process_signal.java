package com.sensicardiac.ekuore.DSP;


import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by davidfourie on 2016/02/03.
 */
public final class Process_signal {
    //class variables---------------------------------------------------------------------------------------------------

    public static RunningStatistics stats=new RunningStatistics();

    public static Signal_buffer sig_buff = new Signal_buffer(3*1050);
    private static MovingAverage ma = new MovingAverage(10);

    private static int RR_low = 450;
    private static int RR_high = 1200;


    private static int HR_low = 50;
    private static int HR_high = 120;
    private static double max1=0;


    private static boolean control=true;
    private static boolean is_stopped=false;
    private static int count3 = 0;



    //class variables-end-----------------------------------------------------------------------------------------------

    //constructor-------------------------------------------------------------------------------------------------------
    private Process_signal() {


    }


    private static double[] linspace(double start, double end, int num_points)
    {
        double[] out = new double[num_points];
//        Log.d("Sound_gen","Length"+num_points);
        double deltaT=(end-start)/num_points;
        double counter=start;
        for(int i=0;i<num_points;i++)
        {
            out[i]=counter;
            counter=counter+deltaT;
        }
        return out;
    }

    //constructor-end---------------------------------------------------------------------------------------------------






    //Add a datapoint to the signal input
    public static void l_add(double input) {

        sig_buff.add_val(input);
//          System.out.println("Adding val");
        count3++;



    }




    //return the trustworthiness of the heartrate calculation


    //Do the signal processing
    public static void process() {



        try {
            //Loop that sets up the trigger points

            if (count3 >= 105) {
//                    Log.d("HR_calc","Processing");
//                System.out.println("testing signal reddiness");
                count3 = 0;

                if (sig_buff.isReady()) {
//

                    Double wind_vect[] = sig_buff.getArray();
//                     System.out.println("array recieved ");

                    for (int i = 0; i < wind_vect.length; i++) {

                        wind_vect[i] = Math.abs(wind_vect[i]) * hammingFunction(i, wind_vect.length);


                    }


                    ProcessingMethods.performFFTConvolution(wind_vect);

                    double local_max = 0;
                    int max_val = 0;

                    for (int i = RR_low; i < RR_high; i++) {

                        if (wind_vect[i] > wind_vect[i + 1] && wind_vect[i] > wind_vect[i - 1] && wind_vect[i] > local_max) {
                            local_max = wind_vect[i];
                            max_val = i;
                        }

                    }


                    if (max_val == 0) {
                        max_val = (RR_low + RR_high) / 2;
                    }


                    local_max = ((double) max_val) / 1050;

                    local_max = 60 / local_max;
                    stats.add_val((double) local_max);
//                        Log.d("HR_calc","Val: "+local_max);
//                        Log.d("HR_calc","Average: "+stats.getMovingAverage());
//                        Log.d("HR_calc","Min: "+stats.getMin());
//                        Log.d("HR_calc","Max: "+stats.getMax());
//                        Log.d("HR_calc","Std: "+stats.getStandardDeviation());
                    ma.add_val(local_max);
                    if (ma.isReady()) {
                        double average = ma.getMovingAverage();

                        if (average > 50.0 && average < 180.0) {
                            RR_low = (int) Math.floor((60 / (average + 10)) * 1050);
                            RR_high = (int) Math.floor((60 / (average - 10)) * 1050);
                        }
                    }

                }

            }

        }catch (Exception e){
            Log.e("Process_Signal Error ", "Signal Process -: " + String.valueOf(e));
        }

    }



    public static void setHR(int low, int high) {
        HR_low = low;
        HR_high = high;


    }





    public static void Stop_processing()
    {
        control=false;
        while(is_stopped);
    }
    public static void Start_processing()
    {
        control=true;
    }


    public static void reset() {


        sig_buff.reset();
        max1=0;
        ma.reset();

        RR_low = (int) Math.floor((60.0 / HR_high) * 1050);
        RR_high = (int) Math.floor((60.0 / HR_low) * 1050);


        stats.reset();
        count3=0;

    }

    //Interface methods-end---------------------------------------------------------------------------------------------
    //class helper methods----------------------------------------------------------------------------------------------
    private static double ShannonEnergy(double in) {
        if (in == 0) {
            in = 0.001;
        }
        return -Math.pow(in, 2) * Math.log10(Math.abs(in));
    }

    private static double hammingFunction(int n, int len) {
        double a = 0.54;
        double b = 0.46;
        return a - b * Math.cos((2 * Math.PI * n) / (len - 1));
    }

    private static double median_list(ArrayList<Double> b) {
        Double a[] = b.toArray(new Double[b.size()]);
        Arrays.sort(a);
        return a[(a.length - 1) / 2];
    }

    private static double Kern(double x) {
        return Math.exp(-1 * Math.pow(x, 2) / 2) / Math.sqrt(2 * Math.PI);
    }

    private static double sum_list(ArrayList<Double> in) {
        double out = 0;
        for (int i = 0; i < in.size(); i++) {
            out = out + in.get(i);

        }
        return out;

    }


//    private static void genHR_locations(int x, double y,int fs)
//    {
//       if(disc_HR.size()<1 || x>next_x)
//       {
//           disc_HR.add(y);
//           disc_HR_loc.add(x);
//           next_x=x+(int)(Math.round(60.0/y)*fs);
//           Log.d("HeartRate","Discrete HR x:"+x+ " y: "+y+" next_x: "+next_x);
//
//       }
//        int diff=signal1.size()*4-gen_signal.size();
//        for(int i=0;i<diff;i++)
//        {
//            gen_signal.add(0.0);
//        }
//        if(gen_signal.size()>4*(disc_HR_loc.lastElement()+265))
//        {
//
//        }
//
//    }

    private static double[] phaseY(double x1,double y1,double x2,double y2,double[] x)
    {
        double m=(y2-y1)/(x2-x1);
        double c=y2-m*x2;
        double out[]= new double[x.length];
        for(int i=0;i<x.length;i++)
        {
            out[i]=m*x[i]+c;
        }
        return out;
    }
    private static double[] betafunc(double a, double b, int n)
    {
        double x[]=linspace(0,1,n);
        for(int i=0;i<n;i++)
        {
            x[i]=Math.pow(x[i],a)*Math.pow(1-x[i],b-1);
        }
        return x;

    }
//    private static void time_clear()
//    {
//        int diff=signal1.size()-30*1000;
//        //remove a number of first elements to trim signal size
////
//
////        for(int i=0;i<diff;i++)
////        {
////            signal1.removeElementAt(0);
////        }
////        Env_processed_level-=diff;
//        if(diff>0) {
//            Log.i("Time_clear","signal size: "+signal1.size());
//            Log.i("Time_clear","HR size: "+HR.size());
//            Log.i("Time_clear","trigger100 size: "+trigger100.size());
//            Log.i("Time_clear","trigger200 size: "+trigger200.size());
//            Log.i("Time_clear","Env_processed_level "+Env_processed_level);
//            Log.i("Time_clear","found peaks size "+found_peaks.size());
//            Log.i("Time_clear","valid peaks size "+valid_peaks.size());
//            Log.i("Time_clear","peak valid  "+peak_valid);
//            Log.i("Time_clear","peak_proc1  "+peak_proc);
//            Log.i("Time_clear","peak_proc2  "+peak_proc2);
//
//        }
//    }



    //class helper methods-end------------------------------------------------------------------------------------------
}
