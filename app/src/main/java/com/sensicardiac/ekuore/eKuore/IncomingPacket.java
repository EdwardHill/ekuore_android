package com.sensicardiac.ekuore.eKuore;

import com.ekuore.ekuorepro.APIeKuorePro.KPacketListener;

import static com.sensicardiac.ekuore.Variables.Recording_Variables.isRecording_busy;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.test_graph1;


/**
 * Created by Edward on 16/09/01.
 */
public  class IncomingPacket implements KPacketListener {
    private static final int SAMPLE_RATE = 44100;
    public static int counter1;
    private int limit  = 256;
    private int graph_size = 512 * limit;
    private int[] graphup = new int [graph_size];
    public static  int offset_graph;
    private static short[] in_Pak;

    public static short[] getIn_Pak() {
        return in_Pak;
    }
    public short[] getPak(){return in_Pak;}
    public static void setIn_Pak(short[] in_Pak) {
        IncomingPacket.in_Pak = in_Pak;
    }

    @Override
    public void newPacket(short[] packet) {
        //Do whatever here
        setIn_Pak(packet);
        if (isRecording_busy()){
        for (int x = 0;x < packet.length;x++) {
            graphup[x + offset_graph] = packet[x];
        }

        counter1++;
        offset_graph +=512;
        if (counter1 == limit || offset_graph == graph_size){
            test_graph1 = graphup;
            counter1 = 0;
            offset_graph = 0;
        }}

    } }