package com.sensicardiac.ekuore.eKuore;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;

import com.ekuore.ekuorepro.APIeKuorePro.AudioRecord;
import com.ekuore.ekuorepro.APIeKuorePro.KeKuorePro;
import com.sensicardiac.ekuore.DSP.Offline_downsampler;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static com.sensicardiac.ekuore.Variables.DSP_Variables.ds;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getAudio_packet;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getByteCombination;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getFinal_audio_packets;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getFinished;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getNumber_of_packets_to_receive;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.isRecording_busy;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setAudio_packet;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setFinal_audio_packets;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setFinished;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setNumber_of_packets_to_receive;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setRecording;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setRecording_busy;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setRecording_done;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setStream_running;
import static com.sensicardiac.ekuore.eKuore.IncomingAudioPacket.reset;



/**
 * Created by Edward on 16/08/31.
 */
public class eKuore_Record {


    public static byte[] reading;
    private static IncomingPacket miRawAudio;
    private static IncomingPacketToDraw miAudioToDraw;
    private static IncomingAudioPacket miAudio;
    public static KeKuorePro miEkuorePro;
    private static Async_eKuore_Record Async_eKuore;
    public static Context con;
    public static String access_point;
    public static String accesspoint_con;
    public static AudioRecord audRec;

   ///TESTING SHORTS

    public static ByteArrayOutputStream byteArrayOutputStream;

    /////
    public static void Start_eKuore_Recording() {
        miEkuorePro = new KeKuorePro();
        byteArrayOutputStream = new ByteArrayOutputStream();
        ds.reset();
        reset();


        access_point = getWifiName(con.getApplicationContext());
        Log.e("AP Point = ", access_point);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here


            if (access_point.contains("ekuorePro_")) {
                accesspoint_con = "ekuorePro_";
//                    +access_point.charAt(12)+access_point.charAt(13)+access_point.charAt(14)+access_point.charAt(15)+access_point.charAt(16)+access_point.charAt(17);
                for (int i = 11; i < 17; i++) {
                    accesspoint_con = accesspoint_con + access_point.charAt(i);
                }
                miEkuorePro.connect(accesspoint_con);//"ekuorePro_dc8d27"
                miRawAudio = new IncomingPacket();
                miAudioToDraw = new IncomingPacketToDraw();
                miAudio = new IncomingAudioPacket();
//                test = new short[1000000];
                miEkuorePro.addPacketListener(miRawAudio);
                miEkuorePro.addPacketListenerToDraw(miAudioToDraw);
                miEkuorePro.addAudioPacketListener(miAudio);


                Log.e("MiEkuorePro", "Get Audio Format :" + miEkuorePro.getAudioFormat());
                Log.e("MiEkuorePro", "Get Channel Nuber :" + miEkuorePro.getChannelNumber());
                Log.e("MiEkuorePro", "Get Sample Rate :" + miEkuorePro.getSampleRate());


                Async_eKuore = (Async_eKuore_Record) new Async_eKuore_Record().execute();
            } else {
                /////TODO Error Message

            }
        }
    }

    public static void Stop_eKuore_Recording() {
        setRecording_busy(false);
        if (Async_eKuore.getStatus() == AsyncTask.Status.RUNNING) {


        }
        if (Async_eKuore.getStatus() == AsyncTask.Status.PENDING) {


        }
        if (Async_eKuore.getStatus() == AsyncTask.Status.FINISHED) {

            Async_eKuore.cancel(true);
            Async_eKuore = null;
        }
        if (String.valueOf(AsyncTask.Status.FINISHED) == getFinished()) {
//            com.sensicardiac.sensi_x_android.Controller.MainDisplay.progress_load.setVisibility(View.VISIBLE);
            miEkuorePro.removePacketListener(miRawAudio);
            miEkuorePro.removePacketListenerToDraw(miAudioToDraw);
            miEkuorePro.removeAudioPacketListener(miAudio);
            try {
                miEkuorePro.disconnect();
            } catch (Exception e) {
                Log.e("Error", " " + e);
            }
            Async_eKuore.cancel(true);
            Async_eKuore = null;
            setRecording(false);
            setStream_running(false);
//            com.sensicardiac.sensi_x_android.Controller.MainDisplay.Record_Stop.setImageResource(R.drawable.ic_record);
//            com.sensicardiac.sensi_x_android.Controller.MainDisplay.Record_Stop_Text.setText("REC");

            setRecording_done(true);
//            com.sensicardiac.sensi_x_android.Controller.MainDisplay.hr_flasher.setVisibility(View.VISIBLE);
            Runtime.getRuntime().gc();
        }

    }

    public static int counter;
    public static int byte_counter;
    public static byte[] total_packets;
    public static short[] draw_pack;
    public static int offSet = 0;
    public static int offSet_aud = 0;

    public static class Async_eKuore_Record extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            offSet_aud = 0;
            offSet = 0;

            IncomingPacket.counter1 = 0;
            IncomingPacket.offset_graph = 0;
            setRecording_done(false);
            reset();
            setAudio_packet(new byte[1024]);
            setNumber_of_packets_to_receive(5168);//Ekuore 600 for 60 seconds
            setFinal_audio_packets(new byte[getAudio_packet().length * getNumber_of_packets_to_receive()]);
            setRecording_busy(true);
            setFinished("RUNNING");
//            recorder = null;

//            byte[] buffer = new byte[128];
            total_packets = getFinal_audio_packets();


            draw_pack = new short[total_packets.length/2];
            try {
                miEkuorePro.startRecording();
            } catch (Exception e) {
                e.printStackTrace();
            }


            int read = 0;
            offSet = 0;
            counter = 0;
            byte_counter = 0;
            File temp = new File("/sdcard/sensi_x/tempfile.wav");

            File dir = new File(Environment.getExternalStorageDirectory() + "/sensi_x");
            if (!dir_exists(dir.getPath())) {
                File directory = new File(String.valueOf(dir));
                directory.mkdirs();
            }
            if (!temp.exists()) {
                try {
                    temp.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (temp.exists()) {
                temp.delete();
                try {
                    temp.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            while (isRecording_busy()) {
                Runtime.getRuntime().gc();
            }

//                try {
//
//
//                    buffer = getEk_packet();
//
////                    for (int i = 0; i < buffer.length; i++) {
//////                        total_packets[i + offSet] = buffer[i];
////                    }
//                    offSet+=1024;
//                    counter++;
//
////                    double temp1 = 0;
//
//                    for (int k = 0; k < 1024; k += 4) {
////                        temp1 = 0;
////                        temp1 += (buffer[k + 1] << 8) + buffer[k];
//                        setLower_byte(buffer[k]);
//                        setUpper_byte(buffer[k + 1]);
////                        ds.add_val(buffer[k],buffer[k + 1]);
////                        add_to_graph();
//                    }
//
//
//
//                    if (counter == getNumber_of_packets_to_receive()) {
//
//                        setRecording_busy(false);
//                    }
//                } catch (Exception e) {
//                    Log.e("Error Async Error", " " + e.getCause());
//
//                }
//
//            }
            try {

                miEkuorePro.stopRecording();




            } catch (Exception e) {
                Log.e("Error", "Stop Recording " + e);
            }


            try {
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }



            new_file =byteArrayOutputStream.toByteArray();
            return "Finished";
        }
        private static byte [] new_file;
        @Override
        protected void onPostExecute(String result) {




            setFinished("FINISHED");
            Stop_eKuore_Recording();
            Offline_downsampler od =new Offline_downsampler();

            od.doDownsample(new_file);
//            Save_Audio(downsample);
        }
    }

    private static byte[] total_bytes;

    private static boolean isOdd(int count) {
        return (count & 0x01) != 0;
    }

    public static void add_to_graph() {
        try {
            double in = getByteCombination();

//            hrCalc.add_val(in);
        } catch (Exception e) {

        }
    }


    public static String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }

    public static boolean dir_exists(String dir_path) {
        boolean ret = false;
        File dir = new File(dir_path);
        if (dir.exists() && dir.isDirectory())
            ret = true;
        return ret;
    }


    private static void writeToFile(String data, Context context, String path) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(path));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private static byte [] ShortToByte_Twiddle_Method(short [] input)
    {
        int short_index, byte_index;
        int iterations = input.length;

        byte [] buffer = new byte[input.length * 2];

        short_index = byte_index = 0;

        for(/*NOP*/; short_index != iterations; /*NOP*/)
        {
            buffer[byte_index]     = (byte) (input[short_index] & 0x00FF);
            buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

            ++short_index; byte_index += 2;
        }

        return buffer;
    }


}
