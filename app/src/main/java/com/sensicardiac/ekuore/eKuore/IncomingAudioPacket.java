package com.sensicardiac.ekuore.eKuore;

import com.ekuore.ekuorepro.APIeKuorePro.AudioPacket;
import com.ekuore.ekuorepro.APIeKuorePro.KAudioPacketListener;

import static com.sensicardiac.ekuore.Variables.DSP_Variables.ds;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.getNumber_of_packets_to_receive;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.isRecording_busy;
import static com.sensicardiac.ekuore.Variables.Recording_Variables.setRecording_busy;
import static com.sensicardiac.ekuore.eKuore.eKuore_Record.byteArrayOutputStream;


/**
 * Created by Edward on 16/09/01.
 */
public class IncomingAudioPacket implements KAudioPacketListener {
    private static byte[] ek_packet;
    private static byte[] ek_packet0;
    private static byte[] ek_packet1;
    private static byte[] ek_packet2;
    private static byte[] ek_packet3;


    private static int counter;

    private static int count_times;
    public static byte[] getEk_packet() {
        return ek_packet;
    }
    public static void reset()
    {
        counter=0;
    }
    public static void setEk_packet(byte[] ek_packet) {
        IncomingAudioPacket.ek_packet = ek_packet;
    }

    @Override
    public void newAudioPacket(AudioPacket audioPacket) {
        setEk_packet(audioPacket.getAudioData());

        if (isRecording_busy()){
        byteArrayOutputStream.write(getEk_packet(),0,getEk_packet().length);
            switch(count_times){
                case 0:
                    ek_packet0 = ek_packet;
                    for (int k = 0; k < 1024; k += 4) {
                        ds.add_val(ek_packet0[k],ek_packet0[k+1]);
                    }
                    break;
                case 3:
                    ek_packet1 = ek_packet;
                    for (int k = 0; k < 1024; k += 4) {
                        ds.add_val(ek_packet1[k],ek_packet1[k+1]);
                    }
                    break;
                case 6:
                    ek_packet2 = ek_packet;
                    for (int k = 0; k < 1024; k += 4) {
                        ds.add_val(ek_packet2[k],ek_packet2[k+1]);
                    }
                    break;
                case 9:
                    ek_packet3 = ek_packet;
                    for (int k = 0; k < 1024; k += 4) {
                        ds.add_val(ek_packet3[k],ek_packet3[k+1]);
                    }
                    break;
                case 10:
                    count_times = 0;
                    break;
            }
//            if(counter%50==0)
//            {
//                Log.d("Packet","Length: "+counter);
//
//            }
//            if (count_times == 0){
//                ek_packet0 = ek_packet;
//                for (int k = 0; k < 1024; k += 4) {
////                        temp1 = 0;
////                        temp1 += (buffer[k + 1] << 8) + buffer[k];
////                setLower_byte(ek_packet[k]);
////                setUpper_byte(ek_packet[k + 1]);
//                    ds.add_val(ek_packet0[k],ek_packet0[k+1]);
////                add_to_graph();
//                }}
//            if (count_times == 3){
//                ek_packet1 = ek_packet;
//                for (int k = 0; k < 1024; k += 4) {
////                        temp1 = 0;
////                        temp1 += (buffer[k + 1] << 8) + buffer[k];
////                setLower_byte(ek_packet[k]);
////                setUpper_byte(ek_packet[k + 1]);
//                    ds.add_val(ek_packet1[k],ek_packet1[k+1]);
////                add_to_graph();
//                }}
//            if (count_times == 6){
//                ek_packet3 = ek_packet;
//                for (int k = 0; k < 1024; k += 4) {
////                        temp1 = 0;
////                        temp1 += (buffer[k + 1] << 8) + buffer[k];
////                setLower_byte(ek_packet[k]);
////                setUpper_byte(ek_packet[k + 1]);
//                    ds.add_val(ek_packet3[k],ek_packet3[k+1]);
////                add_to_graph();
//                }}
//            if (count_times == 9){
//                ek_packet4 = ek_packet;
//                for (int k = 0; k < 1024; k += 4) {
////                        temp1 = 0;
////                        temp1 += (buffer[k + 1] << 8) + buffer[k];
////                setLower_byte(ek_packet[k]);
////                setUpper_byte(ek_packet[k + 1]);
//                    ds.add_val(ek_packet4[k],ek_packet4[k+1]);
////                add_to_graph();
//                }}
            count_times++;
            counter++;
//            Log.d("Packet","Length: "+counter);
//            Log.d("Times","Length: "+count_times);
//            if (count_times >= 10){
//                count_times = 0;
//            }
            if(counter==getNumber_of_packets_to_receive())
            {
                setRecording_busy(false);
            }
        }


    }



}