/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sensicardiac.ekuore.Variables;


import com.sensicardiac.ekuore.DSP.HR_Processor;
import com.sensicardiac.ekuore.DSP.downsampler;

/**
 *
 * @author davidfourie
 */
public class DSP_Variables {
//	public static VisualEnveloper envelope;
	public static HR_Processor hrCalc;
	public static downsampler ds;
	
	
	public static void reset()
	{
		
		hrCalc.reset();
		ds.reset();
//		envelope.reset();
	}
	public static void startDSP()
	{
	System.out.println("Starting DSP");	
//		envelope= new VisualEnveloper();
		 hrCalc= new HR_Processor();
		ds=new downsampler();
	}
}
