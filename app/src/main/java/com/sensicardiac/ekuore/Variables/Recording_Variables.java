package com.sensicardiac.ekuore.Variables;

import java.io.File;

/**
 * Created by Edward on 2016/10/26.
 */

public class Recording_Variables {

    public static boolean Aortic_rec_done;
    public static boolean Pulmonary_rec_done;
    public static boolean Tricuspid_rec_done;
    public static boolean Apex_rec_done;
    public static byte[] Aortic_Array;
    public static byte[] Pulmonary_Array;
    public static byte[] Tricuspid_Array;
    public static byte[] Apex_Array;
    public static byte[] Recording_Array;
    public static File Aortic_File;
    public static File Pulmonary_File;
    public static File Tricuspid_File;
    public static File Apex_File;
    public static File Recording_File;

    private static boolean recording_busy;
    private static String finished;
    private static boolean recording;
    private static boolean stream_running;
    private static boolean recording_done = false;
    private static byte[] audio_packet;
    private static byte[] final_audio_packets;
    private static int number_of_packets_to_receive;
    private static byte upper_byte = 0;
    private static byte lower_byte = 0;
    private static boolean graph_data_ready;

    public static byte[] getFinal_audio_packets() {
        return final_audio_packets;
    }

    public static void setFinal_audio_packets(byte[] final_audio_packets) {
        Recording_Variables.final_audio_packets = final_audio_packets;
    }
    public static int getNumber_of_packets_to_receive() {
        return number_of_packets_to_receive;
    }

    public static void setNumber_of_packets_to_receive(int number_of_packets_to_receive) {
        Recording_Variables.number_of_packets_to_receive = number_of_packets_to_receive;
    }
    public static byte[] getAudio_packet() {
        return audio_packet;
    }

    public static void setAudio_packet(byte[] audio_packet) {
        Recording_Variables.audio_packet = audio_packet;
    }
    public static boolean isRecording_done() {
        return recording_done;
    }

    public static void setRecording_done(boolean recording_done) {
        Recording_Variables.recording_done = recording_done;
    }


    public static boolean isRecording_busy() {
        return recording_busy;
    }

    public static void setRecording_busy(boolean recording_busy) {
        Recording_Variables.recording_busy = recording_busy;
    }

    public static String getFinished() {
        return finished;
    }

    public static void setFinished(String finished) {
        Recording_Variables.finished = finished;
    }

    public static boolean isRecording() {
        return recording;
    }

    public static void setRecording(boolean recording) {
        Recording_Variables.recording = recording;
    }

    public static boolean isStream_running() {
        return stream_running;
    }

    public static void setStream_running(boolean stream_running) {
        Recording_Variables.stream_running = stream_running;
    }

    public static byte getUpper_byte() {
        return upper_byte;
    }

    public static void setUpper_byte(byte upper_byte) {
        Recording_Variables.upper_byte = upper_byte;
    }

    public static byte getLower_byte() {
        return lower_byte;
    }


    public static double getByteCombination()
    {
        return (double) (lower_byte)+(upper_byte<<8);
    }

    public static void setLower_byte(byte lower_byte) {
        Recording_Variables.lower_byte = lower_byte;
    }
    public static int[] test_graph1 = new int[131072];
}
