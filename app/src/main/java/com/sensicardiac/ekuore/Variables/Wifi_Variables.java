package com.sensicardiac.ekuore.Variables;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.sensicardiac.ekuore.Wifi_Controls.WifiScanReceiver;

import java.util.List;

/**
 * Created by Edward on 2016/10/26.
 */

public class Wifi_Variables {
    public static int Network_1;
    public static String Network_1_SSID;
    public static int Stethoscope_1;
    public static String Stethoscope_1_SSID;
    public static int netId;
    public static int stetId;
    public static Spinner lv_1;
    public static Spinner lv_2;
    public static WifiManager wifi;
    public static String wifis[];
    public static String wifis2[];
    public static WifiScanReceiver wifiReciever;
    public static List<ScanResult> wifiScanList;
    public static   List<ScanResult> wifiScanList2;
    public static List<String> wifispinnerArray;
    public static List<String> wifispinnerArray2;
    public static ArrayAdapter<String> adapter;
    public static ArrayAdapter<String> adapter2;
}
